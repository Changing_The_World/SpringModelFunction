package com.personal.quartz.part3;

import com.personal.Main;
import com.personal.quartz.job.HelloDataJob;
import org.junit.jupiter.api.Test;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 *
 * 测试通过原生的Quartz任务写法，获取Spring容器中的bean，并调用bean中的方法。
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:36 上午
 */
@SpringBootTest(classes = Main.class)
public class QuartzSpringBean {

    @Test
    public void getBean(){
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();

            JobDetail job = newJob(HelloDataJob.class)
                    .usingJobData("JobDetail-key-1","JobDetail-value-1")
                    .usingJobData("hehe","a")
                    .withIdentity("job1", "group1")
                    .build();

            Trigger trigger = newTrigger()
                    .usingJobData("Trigger-key-1","Trigger-value-1")
                    .usingJobData("hehe","b")
                    .withIdentity("trigger1", "group1")
                    .startNow()
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule("* * * * * ? *")
                    )
                    .build();

            scheduler.scheduleJob(job, trigger);

            Thread.sleep(3000);
            scheduler.shutdown();
        } catch (SchedulerException | InterruptedException se) {
            se.printStackTrace();
        }
    }
}
