package com.personal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:52 上午
 */
@SpringBootApplication
public class Main {

    @Value("${spring.quartz.properties.org.quartz.scheduler.instanceId}")
    private String instanceId;

    @PostConstruct
    public void printInstanceId(){
        System.out.println("instanceId =========== "+instanceId);
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}
