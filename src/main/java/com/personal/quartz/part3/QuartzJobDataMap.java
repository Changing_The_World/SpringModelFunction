package com.personal.quartz.part3;

import com.personal.quartz.job.HelloDataJob;
import com.personal.quartz.job.HelloJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 *
 * 通过JobDetail或者Trigger 向job中传递参数
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:17 上午
 */
public class QuartzJobDataMap {
    public static void main(String[] args) {
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();

            JobDataMap jobDetailDataMap = new JobDataMap();
            jobDetailDataMap.put("JobDetailDataMap","JobDetail_value");
            JobDetail job = newJob(HelloDataJob.class)
                    .usingJobData("JobDetail-key-1","JobDetail-value-1")
                    .usingJobData("hehe","a")
                    .usingJobData(jobDetailDataMap)
                    .withIdentity("job1", "group1")
                    .build();

            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("Trigger_Job_Data_Map","Trigger_value");
            Trigger trigger = newTrigger()
                    .usingJobData("Trigger-key-1","Trigger-value-1")
                    .usingJobData("hehe","b")
                    .usingJobData(jobDataMap)
                    .withIdentity("trigger1", "group1")
                    .startNow()
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule("* * * * * ? *")
                    )
                    .build();

            scheduler.scheduleJob(job, trigger);

            Thread.sleep(3000);
            scheduler.shutdown();
        } catch (SchedulerException | InterruptedException se) {
            se.printStackTrace();
        }
    }
}
