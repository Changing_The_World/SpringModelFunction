package com.personal.quartz.part2;

import org.quartz.CronExpression;

import java.text.ParseException;

/**
 *
 * 校验表达式规则是否正确
 * 不正确会抛出Exception
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/14 5:15 下午
 */
public class TestCron {
    public static void main(String[] args) throws ParseException {

        //本意：2020年每月1-3号零点执行一次，每月倒数第三天执行一次。但是使用了L就不能再用范围
        CronExpression.validateExpression("0 0 0 1-3,L-3 * ? 2020");

        //这时候需要些两个Trigger来实现这个功能

    }
}
