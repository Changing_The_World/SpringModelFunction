package com.personal.quartz.part2;

import com.personal.quartz.job.HelloJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * 调度器Scheduler --> 触发器Trigger --> 任务详情JobDetail
 *
 *                |--->触发器Trigger  ->任务详情JobDetail|
 * 调度器Scheduler-|--->触发器Trigger |                  |->任务job
 *                |                 |->任务详情JobDetail|
 *                |--->触发器Trigger |
 *
 * 一个调度器可以调度多个触发器，但是一个触发器只能调度一个JobDetail任务，一个JobDetail只能引用一个job。
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/14 2:06 下午
 */
public class Quartz2 {

    public static void main(String[] args) {

        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();
            //job和 Trigger可以只指定name不指定group，如果不指定group默认group名称为"DEFAULT"
            //也可以不指定name，如果不指定name，会自动给name生成一个MD5的值。

            JobDetail job = newJob(HelloJob.class)
                    .withIdentity("job1", "group1")
                    .build();

            Trigger trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInSeconds(3)
                            .repeatForever())
                    .build();

            Trigger trigger2 = newTrigger()
                    .withIdentity("trigger2", "group1")
                    .forJob("job1","group1")
                    .startNow()
                    .withSchedule(
                            simpleSchedule()
                            .withIntervalInSeconds(1)
                            .repeatForever())
                    .build();

            scheduler.scheduleJob(job, trigger);
            scheduler.scheduleJob(trigger2);

            Thread.sleep(3000);
            scheduler.shutdown();
        } catch (SchedulerException | InterruptedException se) {
            se.printStackTrace();
        }
    }

}
