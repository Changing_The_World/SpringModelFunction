package com.personal.quartz.job;

import com.personal.quartz.service.HelloService;
import com.personal.quartz.util.SpringContextUtil;
import org.quartz.*;
import org.springframework.util.StringUtils;

/**
 *
 * 通过JobDetail或者Trigger 向job中传递参数
 * 任务启动后获取Spring容器，并从容器中获取bean，调用bean的方法
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:15 上午
 */
public class HelloDataJob implements Job {
    HelloService helloService;
    private String hehe;

    public void setHehe(String hehe) {
        this.hehe = hehe;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
//        helloService = (HelloService) SpringContextUtil.applicationContext.getBean(StringUtils.uncapitalize(HelloService.class.getSimpleName()));
//        System.out.println(helloService.Hello());

        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        JobDataMap jobDataMap1 = jobExecutionContext.getTrigger().getJobDataMap();

        System.out.println(jobDataMap.get("JobDetail-key-1"));
        System.out.println(jobDataMap1.get("Trigger-key-1"));

        //输出b ， 说明trigger中的参数优先级高于JobDetail中的参数
        System.out.println(jobExecutionContext.getMergedJobDataMap().get("hehe"));

        //获取传入的JobDataMap集合中的参数
        System.out.println(jobDataMap.get("JobDetailDataMap"));
        System.out.println(jobDataMap1.get("Trigger_Job_Data_Map"));

        //对于那些必传参数，或者常用参数，可以在job中定义成员变量，给成员变量设置set方法
        // 任务启动后会自动的从JobDataMap中注入到Job任务对象中。
        System.out.println(" -=-=-=-= "+hehe);
    }
}
