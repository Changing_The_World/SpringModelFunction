package com.personal.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.StringJoiner;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/1/14 2:16 下午
 */
public class HelloJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        StringJoiner out = new StringJoiner("hello job ")
                .add(System.currentTimeMillis()+"")
                .add(jobExecutionContext.getTrigger().getKey().getName());
        System.out.println(out);
    }
}
