package com.personal.quartz.part4;

import com.personal.quartz.job.HelloJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 *
 * 在quartz.properties 配置文件中可以修改quartz配置信息。
 * 并在Scheduler中获取quartz配置信息
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/16 11:00 下午
 */
public class QuartzProperties {
    public static void main(String[] args) throws SchedulerException, InterruptedException {
        Scheduler defaultScheduler = StdSchedulerFactory.getDefaultScheduler();
        defaultScheduler.start();

        System.out.println(defaultScheduler.getSchedulerName());
        System.out.println("线程个数 == "+defaultScheduler.getMetaData().getThreadPoolSize());

        JobDetail job = JobBuilder.newJob(HelloJob.class)
                .withIdentity("job","g1")
                //Schedule绑定job的另一种方式**
//                .storeDurably()
                .build();

        Trigger trigger = newTrigger()
                .withIdentity("trigger","t1")
                .forJob("job","g1")
                .startNow()
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("* * * * * ? *")
                ).build();


        //scheduler绑定job的另一种方式**
//        defaultScheduler.addJob(job,false);
        defaultScheduler.scheduleJob(job,trigger);

        Thread.sleep(3000);
        defaultScheduler.shutdown();


    }
}
