package com.personal.quartz.part6;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.StringJoiner;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/1/18 2:50 下午
 */
public class ClusterJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        StringJoiner out = new StringJoiner("----");
        out.add("Cluster-Job-Test")
                .add(jobExecutionContext.getJobDetail().getKey().getName());
        System.out.println(out);
    }
}
