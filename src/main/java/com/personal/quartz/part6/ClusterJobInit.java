package com.personal.quartz.part6;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 *
 * job集群演示：
 * 1.quartz集群必须配有持久化数据源，并且集群的各个节点共用数据源
 * 2.同一套job任务，在多个节点启动，这一组job会由quartz分配执行在哪个节点上。
 *
 * 存在bug:
 * 1.initialize-schema: always 这样第一个服务启动时，删除表重建，持久化job信息到表；第二个服务启动时，删除表重建，持久化job信息到表。这样导致job任务执行时间乱序
 * 解：可以手动初始化数据库表，初始化脚本在quartz包里-"table_mysql.sql"。 把initialize-schema值改成never
 *
 * 2.第一次集群启动-停止后，job任务被记录到数据库，第二次集群再启动，从数据库查询任务并执行，此时又执行了jobInit()初始化了相同的任务，产生冲突，从而报错。
 * 解：方式一（推荐）：工程中写一个Controller，程序启动后，手动调用一次Controller，初始化job任务。 之后启停程序都从数据库中读取job任务。相同的任务：相同的job-Trigger组合
 *    方式二（不推荐）：
 *    scheduler.scheduleJob(job,trigger);
 *   改成：
 *    Set<Trigger> set = new HashSet<>();
 *    set.add(trigger);
 *    scheduler.scheduleJob(job, set,true); //true表示把应用中的任务用数据库中的替换掉
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/18 2:44 下午
 */
@Component
public class ClusterJobInit {

    /**
     * 配置文件中，instanceName表示集群名称，不同的集群名称表示不同的集群。当同一个集群节点数量增加时，quartz调度任务性能会下降，
     * 这时，考虑将quartz程序分成多个集群：
     * 1.假设任务节点共100个，job任务1000个。将一个集群分成两个集群。
     * 2.每50个节点组成一个集群，负责500个job任务，集群名称 spring.quartz.properties.org.quartz.scheduler.instanceName:OrderService-1
     *                                              spring.quartz.properties.org.quartz.scheduler.instanceName:OrderService-2
     * 3.将集群名称设置到JobDetail和Trigger的group名称上，初始化任务时，给每个job任务指定集群名，if !groupName.eq(instanceName) return;
     */
    @Value("${spring.quartz.properties.org.quartz.scheduler.instanceName}")
    public String instanceName;




    @Autowired
    public Scheduler scheduler;

    @PostConstruct
    public void jobInit() throws SchedulerException {
        buildJob("job-1","trigger-1","OrderService-1");
        buildJob("job-2","trigger-2","OrderService-2");
        buildJob("job-3","trigger-3","OrderService-2");
        buildJob("job-4","trigger-4","OrderService-1");
    }

    public void buildJob(String jobName,String triggerName,String group) throws SchedulerException {
        if (!instanceName.equals(group)){
            return;
        }
        JobDetail job = JobBuilder.newJob(ClusterJob.class)
                .withIdentity(jobName,group)
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerName,group)
                .startNow()
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(3))
                .build();

        scheduler.scheduleJob(job,trigger);

//        Set<Trigger> set = new HashSet<>();
//        set.add(trigger);
//        scheduler.scheduleJob(job, set,true);
    }
}
