package com.personal.quartz.part5;

import com.personal.quartz.service.HelloService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.StringJoiner;

/**
 *
 * Spring 继承Quartz
 * 1.extends QuartzJobBean 重写executeInternal方法，里边些job的业务逻辑
 * 2.创建JobInit类，注入Scheduler、手动创建JobDetail和Trigger，并将jobDetail和Trigger碧昂顶到scheduler上。
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/16 11:38 下午
 */
public class SpringJob extends QuartzJobBean {

    @Autowired
    private HelloService helloService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        StringJoiner out = new StringJoiner("|")
                .add(Thread.currentThread().getName())
                .add("SpringJob.executeInternal")
                .add(helloService.toString())
                .add(helloService.Hello())
                .add(context.getJobDetail().getJobDataMap().getString("JobDetail"));
        System.out.println(out);
    }
}
