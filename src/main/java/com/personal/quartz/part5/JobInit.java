package com.personal.quartz.part5;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/1/16 11:42 下午
 */
//@Component
public class JobInit {

    @Autowired
    public Scheduler scheduler;

    /**
     * @PostContruct
     * 是Java自带的注解，在方法上加该注解会在项目启动的时候执行该方法，也可以理解为在spring容器初始化的时候执行该方法。
     * 从Java EE5规范开始，Servlet中增加了两个影响Servlet生命周期的注解，@PostConstruct和@PreDestroy，
     * 这两个注解被用来修饰一个非静态的void（）方法。
     * @throws SchedulerException
     */
    @PostConstruct
    public void initJob() throws SchedulerException {
        JobDetail jobDetail = JobBuilder.newJob(SpringJob.class)
                .usingJobData("JobDetail","PersonalJobDetail")
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .build();

        scheduler.scheduleJob(jobDetail, trigger);
    }

}
