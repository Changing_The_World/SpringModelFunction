package com.personal.quartz.part5;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * Spring 工具的创建jobDetail和Trigger的写法
 *
 * 如果Bean比较多，可能分散在多个文件，不好管理，也不容器看清jobDetail和Trigger的关联
 * 这种写法要考虑好每个Bean的名称（默认是方法名作为bean的名称）
 *
 * 设置quartz持久化的数据源连接信息
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/16 11:42 下午
 */
//@Component
public class JobConfig {

    /**
     * 这是给quartz单独配置DataSource的第二种方式，提供一个DataSource的Bean。
     * @return
     */
//    @Bean
//    @QuartzDataSource
//    public DataSource qDataSource(){
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setUsername("");
//        dataSource.setPassword("");
//        dataSource.setUrl("");
//        return dataSource;
//    }

    @Bean
    public JobDetail springJobDetail(){
        return JobBuilder.newJob(SpringJob.class)
                .usingJobData("JobDetail","SpringJobDetail")
                .withIdentity("job_detail1")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger springTrigger(){
        return TriggerBuilder.newTrigger()
                .forJob("job_detail1")
                .startNow()
                .build();
    }

}
