package com.personal.quartz.service;

import org.springframework.stereotype.Service;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:37 上午
 */
@Service
public class HelloService {

    public String Hello(){
        return "hello Quartz Service!!!";
    }
}
