package com.personal.quartz.util;

import org.quartz.CronExpression;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 *
 * 从这个类里可以手动获取Spring容器，进而可以从容器中手动的获取Bean
 *
 * @Author: guo chunyu
 * @DateTime: 2022/1/15 12:40 上午
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {
    public static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextUtil.applicationContext = applicationContext;
    }

    public static boolean checkCron(String cron){
        return CronExpression.isValidExpression(cron);
    }

    public static void main(String[] args) {
        System.out.println(checkCron(""));
        System.out.println(checkCron("* * * * * ? *"));
    }
}
